import hashlib
import io
import re
import httpx
import logging
import conda.exports
import networkx
from typing import List, Dict, Any, Optional
from pydantic import BaseModel
from gidgetlab import GitLabException
from gidgetlab.httpx import GitLabAPI
from ruamel_yaml import YAML
from gidgetlab_cli.models import Project, GitLabId
from gidgetlab_cli import api
from .exceptions import InvalidRecipe


logger = logging.getLogger(__name__)


class Package(BaseModel):
    """Conda Package"""

    name: str
    version: str
    max_pin: Optional[str] = None


def get_index(channel: str, platform: str) -> Dict[Any, Any]:
    """Return the index of the packages available on the conda channel"""
    index = conda.exports.get_index((channel,), prepend=False, platform=platform)
    return index


def make_graph(channel: str, platform: str) -> networkx.DiGraph:
    """Return a directed graph with all packages from channel and their dependencies"""
    index = get_index(channel, platform)
    graph = networkx.DiGraph()
    for package in index.values():
        name = package["name"]
        graph.add_node(name)
        for dep in package.get("depends", []):
            # depends is a tuple of "name version" strings
            # depends=('epics-base >=3.15.5,<3.15.6.0a0', 'libgcc-ng >=7.3.0')
            (dep_name, _, _) = dep.partition(" ")
            graph.add_edge(name, dep_name)
    return graph


def get_direct_reverse_dependencies(
    name: str, channel: str, platform: str
) -> List[str]:
    """Return a sorted list of direct reverse dependencies for name

    Remove packages that are also ancestors of a predecessor
         A - G
        /|\
       D B C
       | |
       E C
       |
       G
    The direct reverse dependencies of A are B and D. C would be removed because it depends on B.
    G would also be removed because it's an ancestor of D (via E).
    """
    graph = make_graph(channel, platform)
    try:
        predecessors = list(graph.predecessors(name))
    except networkx.NetworkXException as e:
        logger.warning(e)
        return []
    predecessors_ancestors = {
        ancestor
        for package in predecessors
        for ancestor in networkx.algorithms.dag.ancestors(graph, package)
    }
    return sorted(
        [package for package in predecessors if package not in predecessors_ancestors]
    )


async def create_project_trigger(
    gl: GitLabAPI, project_id: GitLabId, description: str = "conda-bot trigger"
) -> str:
    """Create a project trigger and return the corresponding token"""
    result = await gl.post(
        f"/projects/{project_id}/triggers", data={"description": description}
    )
    return result["token"]


async def get_project_triggers(
    gl: GitLabAPI, project_id: GitLabId
) -> List[Dict[str, str]]:
    """Return the project triggers"""
    return [trigger async for trigger in gl.getiter(f"/projects/{project_id}/triggers")]


async def get_project_trigger_token(
    gl: GitLabAPI, project_id: GitLabId, description: str = "conda-bot trigger"
) -> str:
    """Return a trigger token for the project

    If no existing token is found, one is created
    """
    triggers = await get_project_triggers(gl, project_id)
    tokens = [
        trigger["token"]
        for trigger in triggers
        if trigger["description"] == description
    ]
    if tokens:
        return tokens[0]
    return await create_project_trigger(gl, project_id, description)


async def trigger_pipeline(
    gl: GitLabAPI, project_id: GitLabId, branch: str, token: str
) -> None:
    """Trigger the project pipeline"""
    await gl.post(
        f"/projects/{project_id}/trigger/pipeline",
        params={"token": token, "ref": branch},
        data={},
    )


async def list_branches(gl: GitLabAPI, project_id: GitLabId) -> List[str]:
    """Return the list of branches name"""
    return [
        branch["name"]
        async for branch in gl.getiter(f"/projects/{project_id}/repository/branches")
    ]


async def trigger_pipelines(gl: GitLabAPI, projects: List[Project]) -> None:
    """Trigger the pipeline for all projects"""
    for project in projects:
        try:
            token = await get_project_trigger_token(gl, project.id)
        except GitLabException as e:
            logger.error(f"Couldn't get project {project.name} trigger token: {e}")
        else:
            branches = await list_branches(gl, project.id)
            for branch in branches:
                await trigger_pipeline(gl, project.id, branch, token)


async def get_archive_sha256(project: Project, tag: str) -> str:
    """Return the sha256 of the project archive"""
    # The archive API (/projects/{project_id}/repository/archive) doesn't return the same file
    # as the one downloaded from the UI (it includes the commit sha in the repo name)
    async with httpx.AsyncClient() as client:
        r = await client.get(
            f"{project.web_url}/-/archive/{tag}/{project.name}-{tag}.tar.gz"
        )
    r.raise_for_status()
    return hashlib.sha256(r.content).hexdigest()


def update_recipe_meta_yaml(content: str, version: str, sha256: str) -> str:
    new = re.sub(r"set version = \".*\"", f'set version = "{version}"', content)
    new = re.sub(r"sha256:.*\n", f"sha256: {sha256}\n", new)
    # Reset build number to 0 when changing version
    new = re.sub(r"number:.*\n", "number: 0\n", new)
    return new


async def get_yaml_file_from_repository(
    gl: GitLabAPI, project_id: GitLabId, file_path: str, ref: str
) -> Dict[str, Any]:
    """Load the yaml file from the repository"""
    content = await api.get_file_from_repository(gl, project_id, file_path, ref)
    yaml = YAML(typ="rt")
    return yaml.load(content)


def get_package_info(meta_yaml_content: str) -> Package:
    """Get the package name, version and max_pin from the meta.yaml content"""
    m = re.search(r"set name = \"(.*)\"", meta_yaml_content)
    if m:
        name = m.group(1).lower()
    else:
        m = re.search(r"name:\s*(.*)$", meta_yaml_content, re.M)
        if m:
            name = m.group(1)
        else:
            raise InvalidRecipe("Couldn't get the name from the recipe")
    m = re.search(r"set version = \"(.*)\"", meta_yaml_content)
    if m:
        version = m.group(1)
    else:
        m = re.search(r"version:\s*(.*)$", meta_yaml_content, re.M)
        if m:
            version = m.group(1)
        else:
            raise InvalidRecipe("Couldn't get the version from the recipe")
    m = re.search(r"pin_subpackage.*max_pin=(.*)\)", meta_yaml_content)
    if m:
        # Remove the single or double quotes around the string
        max_pin: Optional[str] = m.group(1).strip("'").strip('"')
    else:
        max_pin = None
    return Package(name=name, version=version, max_pin=max_pin)


def get_pin_version(version: str, max_pin: Optional[str]) -> Optional[str]:
    """Return the version to pin based on max_pin

    This is a simplified function compare to what conda does
    It only handles semantic versions
    """
    if max_pin is None:
        return None
    pin_nb = len(max_pin.split("."))
    return ".".join(version.split(".")[:pin_nb])


async def get_package_info_from_repository(
    gl: GitLabAPI, project_id: GitLabId, ref: str = "master"
) -> Package:
    """Get the package name and version from the recipe repository"""
    meta_yaml_content = await api.get_file_from_repository(
        gl, project_id, "recipe/meta.yaml", ref="master"
    )
    return get_package_info(meta_yaml_content)


async def create_recipe_mr(
    gl: GitLabAPI, project_id: GitLabId, version: str, archive_sha256: str, user_id: int
) -> None:
    """Create a merge request to update the recipe"""
    file_path = "recipe/meta.yaml"
    branch = f"update_to_{version}"
    content = await api.get_file_from_repository(gl, project_id, file_path, "master")
    updated_content = update_recipe_meta_yaml(content, version, archive_sha256)
    await api.update_file_via_merge_request(
        gl,
        project_id,
        file_path,
        updated_content,
        branch,
        "master",
        f"Update version to {version}",
        assignee_id=user_id,
        merge_on_success=True,
        wait_on_merge=False,
    )


def update_conda_build_config(
    conda_build_config_yaml: Dict[str, Any], name: str, version: str
) -> bool:
    # The name of the package in conda_build_config.yaml must be a valid Python variable
    # and cannot contain "-". Dashes are replaced with "_".
    name = name.replace("-", "_")
    try:
        versions = conda_build_config_yaml[name]
    except KeyError:
        logger.info(f"Add {name} to conda_build_config.yaml (pinned to {version})")
        conda_build_config_yaml[name] = [version]
        return True
    if version in versions:
        logger.info(f"{name} already pinned to {version}")
        return False
    if len(versions) == 1:
        logger.info(f"Pin {name} to {version} instead of {versions[0]}")
        conda_build_config_yaml[name] = [version]
        return True
    else:
        raise NotImplementedError(
            f"{name} pinned to multiple versions: {versions}. "
            "Pinning update not implemented for multiple versions."
        )
    return False


def yaml_to_string(data: Dict[str, Any]) -> str:
    yaml = YAML(typ="rt")
    yaml.indent(mapping=2, sequence=4, offset=2)
    output = io.StringIO()
    yaml.dump(data, output)
    content = output.getvalue()
    output.close()
    return content


async def update_pinning(
    gl: GitLabAPI, project_id: GitLabId, global_pinning_project_id: GitLabId
) -> None:
    """Update the global conda_build_config.yaml file with the latest version of project"""
    # Get the package name and version from the master branch of the recipe
    package = await get_package_info_from_repository(gl, project_id)
    pin_version = get_pin_version(package.version, package.max_pin)
    if pin_version is None:
        logger.info(f"No version to pin for {package.name} (no max_pin found)")
        return
    # Get the global conda_build_config.yaml
    conda_build_config_yaml = await get_yaml_file_from_repository(
        gl, global_pinning_project_id, "conda_build_config.yaml", "master"
    )
    # Update the file
    update = update_conda_build_config(
        conda_build_config_yaml, package.name, pin_version
    )
    if update:
        branch = f"pin_{package.name}_to_{pin_version}"
        content = yaml_to_string(conda_build_config_yaml)
        await api.update_file_via_merge_request(
            gl,
            global_pinning_project_id,
            "conda_build_config.yaml",
            content,
            branch,
            "master",
            f"Pin {package.name} to {pin_version}",
            merge_on_success=True,
            wait_on_merge=True,
        )
