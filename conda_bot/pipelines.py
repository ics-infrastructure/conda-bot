import gidgetlab.routing
from typing import Any
from gidgetlab.abc import GitLabAPI
from gidgetlab.sansio import Event
from gidgetlab_cli.models import Project
from .tasks import trigger_reverse_dependencies_pipelines

router = gidgetlab.routing.Router()


@router.register("Pipeline Hook")
async def trigger_reverse_dependencies_build(
    event: Event, gl: GitLabAPI, *args: Any, **kwargs: Any
) -> None:
    """Trigger reverse dependencies build"""
    status = event.object_attributes["status"]
    if status != "success":
        return
    # When pushing to a branch to create a MR, only the build stage is executed
    # There is no new pipeline to trigger in this case as no new package was released
    stages = event.object_attributes["stages"]
    if "release" not in stages:
        return
    project = Project(**event.data["project"])
    await trigger_reverse_dependencies_pipelines(gl, project, True)
