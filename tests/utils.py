"""Utilities for tests"""
import json
import urllib
import pathlib

SAMPLES_PATH = pathlib.Path(__file__).parent / "data" / "samples"


def sample(name):
    file_path = SAMPLES_PATH / name
    with file_path.open("r") as f:
        data = json.load(f)
    return data


class FakeGitLab:
    def __init__(self, *, getiter=None, getitem=None, post=None, put=None):
        self._getitem_return = getitem
        self._getiter_return = getiter
        self._post_return = post
        self._put_return = put
        self.getitem_url = None
        self.getiter_url = []
        self.post_url = []
        self.post_data = []
        self.post_params = []
        self.put_url = []
        self.put_data = []
        self.getitem_calls = 0
        self.getiter_calls = 0

    async def getitem(self, url, params=None):
        self.getitem_calls += 1
        self.getitem_url = url
        if params:
            self.getitem_url += f"?{urllib.parse.urlencode(params)}"
        return self._getitem_return

    async def getiter(self, url):
        self.getiter_url.append(url)
        if self._getiter_return and isinstance(self._getiter_return[0], (list, tuple)):
            items = self._getiter_return[self.getiter_calls]
        else:
            items = self._getiter_return
        self.getiter_calls += 1
        for item in items:
            yield item

    async def post(self, url, *, data, params=None):
        self.post_url.append(url)
        self.post_data.append(data)
        self.post_params.append(params)
        return self._post_return

    async def put(self, url, *, data):
        self.put_url.append(url)
        self.put_data.append(data)
        return self._put_return

    async def sleep(self, seconds):
        return None
