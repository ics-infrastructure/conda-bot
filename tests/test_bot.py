import pytest
import gidgetlab.routing
from unittest.mock import Mock
from starlette.testclient import TestClient
from conda_bot.bot import GitLabBot


@pytest.fixture()
def client():
    """
    Make a 'client' fixture available to test cases.
    """
    app = GitLabBot("conda-bot")
    # Our fixture is created within a context manager. This ensures that
    # application startup and shutdown run for every test case.
    with TestClient(app) as test_client:
        yield test_client


def test_valid_webhook_request(client):
    headers = {"x-gitlab-event": "Issue Hook"}
    data = {"action": "open"}
    # No event is registered, so no callback will be triggered,
    # but no error should be raised
    response = client.post("/", headers=headers, json=data)
    assert response.status_code == 200


@pytest.mark.parametrize(
    "headers, status_code, detail",
    [
        ({}, 500, "x-gitlab-token is missing"),
        ({"x-gitlab-token": "foo"}, 500, "invalid secret"),
    ],
)
def test_webhook_request_invalid_header(headers, status_code, detail):
    app = GitLabBot("conda-bot", secret="12345")
    with TestClient(app, raise_server_exceptions=False) as client:
        response = client.post("/", headers=headers, json={})
    assert response.status_code == status_code
    assert response.text == detail


def test_webhook_request_invalid_media_type():
    app = GitLabBot("conda-bot")
    with TestClient(app, raise_server_exceptions=False) as client:
        response = client.post("/", headers={})
    assert response.status_code == 415
    assert response.text.startswith("expected a content-type of 'application/json'")


def test_webhook_handler_triggered(client):
    handler_mock = Mock()

    @client.app.webhook_router.register("Issue Hook", action="open")
    async def issue_opened_event(event, gl, *args, **kwargs):
        handler_mock()

    # First send a request that should not trigger the handler
    headers = {"x-gitlab-event": "Push Hook"}
    data = {"object_kind": "push"}
    response = client.post("/", headers=headers, json=data)
    assert response.status_code == 200
    assert not handler_mock.called

    # Send a request that should trigger the handler
    headers = {"x-gitlab-event": "Issue Hook"}
    data = {"object_kind": "issue", "object_attributes": {"action": "open"}}
    response = client.post("/", headers=headers, json=data)
    assert response.status_code == 200
    assert handler_mock.called


def test_init_routers():
    issue_router = gidgetlab.routing.Router()
    issue_mock = Mock()
    push_router = gidgetlab.routing.Router()
    push_mock = Mock()

    @issue_router.register("Issue Hook", action="open")
    async def issue_opened_event(event, gl, *args, **kwargs):
        issue_mock()

    @push_router.register("Push Hook")
    async def push_event(event, gl, *args, **kwargs):
        push_mock()

    app = GitLabBot("conda-bot", routers=[issue_router, push_router])
    with TestClient(app) as client:
        headers = {"x-gitlab-event": "Push Hook"}
        data = {"object_kind": "push"}
        response = client.post("/", headers=headers, json=data)
        assert response.status_code == 200
        assert not issue_mock.called
        assert push_mock.called

        headers = {"x-gitlab-event": "Issue Hook"}
        data = {"object_kind": "issue", "object_attributes": {"action": "open"}}
        response = client.post("/", headers=headers, json=data)
        assert response.status_code == 200
        assert issue_mock.called


def test_health(client):
    """The server should answer 'Bot OK' on /health endpoint"""
    response = client.get("/health")
    assert response.status_code == 200
    assert response.text == "Bot OK"
