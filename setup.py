# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

requirements = [
    "aiocache",
    "cachetools",
    "gidgetlab[httpx]",
    "gunicorn",
    "networkx",
    "pydantic",
    "sentry-sdk",
    "starlette",
    "uvicorn",
]

test_requirements = [
    "pytest>=3.0.0",
    "pytest-cov",
    "pytest-asyncio",
    "pytest-mock",
    "respx==0.15.1",
]

setup(
    name="conda-bot",
    author="Benjamin Bertrand",
    author_email="benjamin.bertrand@ess.eu",
    description="GitLab bot to manage conda repositories",
    url="https://gitlab.esss.lu.se/ics-infrastructure/conda-bot",
    license="MIT license",
    version="0.8.0",
    install_requires=requirements,
    tests_require=test_requirements,
    packages=find_packages(exclude=("tests", "tests.*")),
    include_package_data=True,
    keywords="gitlab",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
    ],
    extras_require={"tests": test_requirements},
    python_requires=">=3.8.0",
)
